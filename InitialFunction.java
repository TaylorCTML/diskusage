import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Properties;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/*
 * Disk Space Monitoring 0.2
 * Author: Taylor Cook
 * Date created: 2018-07-29
 * Last updated: 2018-07-30
 * Purpose: Uses a predetermined text file with disk usage data in them and parses it and generates a daily email to alert *operators of any issues that may need to be resolved.
 */


public class InitialFunction {

	private static StringBuilder emailBody;

	public static void main(String[] args) {
		ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				emailBody = new StringBuilder();
				String caviarNetworkLocation = "\\\\caviar\\freespace.txt";
				Path caviarPath = Paths.get(caviarNetworkLocation);
				DatabaseServer caviar;
				if (Files.exists(caviarPath)) {
					caviar = new DatabaseServer(caviarPath, "caviar");
				} else {
					emailBody.append("caviar is not reachable or the data file does not exist.").append((System.getProperty("line.separator")));
					caviar = null;
				}
				String merlotNetworkLocation = "\\\\merlot\\freespace.txt";
				DatabaseServer merlot;
				Path merlotPath = Paths.get(merlotNetworkLocation);
				if (Files.exists(merlotPath)) {
					merlot = new DatabaseServer(merlotPath, "merlot");
				} else {
					emailBody.append("merlot is not reachable or the data file does not exist.").append((System.getProperty("line.separator")));
					merlot = null;
				}
				String tml2014NetworkLocation = "\\\\tml2014\\freespace.txt";
				DatabaseServer tml2014;
				Path tml2014path = Paths.get(tml2014NetworkLocation);
				if (Files.exists(tml2014path)) {
					tml2014 = new DatabaseServer(tml2014path, "tml2014");
				} else {
					emailBody.append("tml2014 is not reachable or the data file does not exist.").append((System.getProperty("line.separator")));
					tml2014 = null;
				}
				String AS2NetworkLocation = "\\\\AS2\\freespace.txt";
				DatabaseServer AS2;
				Path AS2path = Paths.get(AS2NetworkLocation);
				if (Files.exists(AS2path)) {
					AS2 = new DatabaseServer(Paths.get(AS2NetworkLocation), "AS2");
				} else {
					emailBody.append("AS2 is not reachable or the data file does not exist.").append((System.getProperty("line.separator")));
					AS2 = null;
				}
				ArrayList<DatabaseServer> databaseServers = new ArrayList<>();
				if (Objects.nonNull(caviar)) {
					databaseServers.add(caviar);
				}
				if (Objects.nonNull(merlot)) {
					databaseServers.add(merlot);
				}
				if (Objects.nonNull(tml2014)) {
					databaseServers.add(tml2014);
				}
				if (Objects.nonNull(AS2)) {
					databaseServers.add(AS2);
				}
				if (databaseServers.size() > 0) {
					for (DatabaseServer databaseServer : databaseServers) {
						try {
							databaseServer.getDiskSpace();
						} catch (Exception e) {
							StringWriter sw = new StringWriter();
							PrintWriter pw = new PrintWriter(sw);
							e.printStackTrace(pw);
							String sStackTrace = sw.toString();
							emailBody = new StringBuilder();
							emailBody.append("Warning: an error has occurred and the stacktrace has been sent to Taylor to fix.");
							sendEmail("ERROR", sStackTrace);
							break;
						}
						emailBody.append(databaseServer.generateEmailBody());
					}
					StringBuilder startEmailBody = new StringBuilder();
					startEmailBody.append("Last updated disk usage space on ")
							.append(databaseServers.get(0).getDayOfCheck())
							.append(" at ")
							.append(databaseServers.get(0).getTimeOfDay())
							.append(".")
							.append((System.getProperty("line.separator")))
							.append((System.getProperty("line.separator")))
							.append(emailBody); //building the email
					String body = startEmailBody.toString();
					String subject = LocalDate.now() + ": Disk Space Report";
					sendEmail(subject, body);
				} else {
					String body = emailBody.toString();
					String subject = LocalDate.now() + ": Disk Space Report";
					sendEmail(subject, body);
				}
			}

		};
		long initialDelay = getInitialDelay();
		ScheduledFuture<?> scheduledFuture = executorService.scheduleWithFixedDelay(runnable, initialDelay, 1440, TimeUnit.MINUTES);
	}

	private static void sendEmail(String subject, String body) {
		String username = "marketsas2notification@gmail.com";
		String password = "";
		String toEmailAddress = "taylorcook@themarketsllc.com";

		Properties properties = new Properties();
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.host", "smtp.gmail.com");
		properties.put("mail.smtp.port", "587");

		Authenticator authenticator = new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		};

		Session session = Session.getInstance(properties, authenticator);

		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(username));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmailAddress));
			message.setSubject(subject);
			message.setText(body);
			Transport.send(message);
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}

	/*function to retrieve how much time is remaining till 5am the next day. so that if the program crashes or is started at another time, that it doesn't spam people with notifications. */

	private static long getInitialDelay() {
		LocalDateTime now = LocalDateTime.now();
		LocalDate dayToCalculate = LocalDate.now();
		LocalTime timeOfDay = LocalTime.of(5, 5);
		LocalDateTime timeToCheck = LocalDateTime.of(dayToCalculate, timeOfDay);
		Duration duration = Duration.between(now, timeToCheck);
		if (duration.isNegative()) {
			LocalDate dayToCalculateNew = dayToCalculate.plusDays(1);
			timeToCheck = LocalDateTime.of(dayToCalculateNew, timeOfDay);
			duration = Duration.between(now, timeToCheck);
		}
		return duration.toMinutes();
	}
}