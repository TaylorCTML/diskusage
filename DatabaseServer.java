import java.math.BigInteger;
import java.nio.file.Path;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Scanner;

/*
 * Disk Space Monitoring 0.1
 * Author: Taylor Cook
 * Last updated: 2018-07-29
 *
 */

public class DatabaseServer {

	private Path pathToOutputFile;
	private String serverName;
	private ArrayList<DriveLetters> driveLetters;
	private LocalDate dayOfCheck;
	private LocalTime timeOfDay;

	public DatabaseServer(Path pathToOutputFile, String serverName) {
		this.pathToOutputFile = pathToOutputFile;
		this.serverName = serverName;
		driveLetters = new ArrayList<>();
		if (serverName.equalsIgnoreCase("merlot")) {
			driveLetters.add(new DriveLetters('C', serverName));
			driveLetters.add(new DriveLetters('D', serverName));
			driveLetters.add(new DriveLetters('E', serverName));
		} else if (serverName.equalsIgnoreCase("caviar")) {
			driveLetters.add(new DriveLetters('C', serverName));
			driveLetters.add(new DriveLetters('D', serverName));
			driveLetters.add(new DriveLetters('F', serverName));
		} else {
			driveLetters.add(new DriveLetters('C', serverName));
		}

	}

	public ArrayList<DriveLetters> getDriveLetters() {
		return driveLetters;
	}

	LocalDate getDayOfCheck() {
		return dayOfCheck;
	}

	LocalTime getTimeOfDay() {
		return timeOfDay;
	}
/*
Method for taking the disk usage results and adding them into an email.
*/
	public String generateEmailBody() {
		String emailLines;
		StringBuilder result = new StringBuilder();
		for (DriveLetters driveLetter : driveLetters) {
			result.append(driveLetter.toString());
			result.append((System.getProperty("line.separator")));
		}
		return emailLines = result.toString();
	}

	public void getDiskSpace() throws Exception {
		Path path = pathToOutputFile;
		Scanner sc = new Scanner(path);
		String[] parsedText = new String[5];
		int lineNumber = 0;
		while (sc.hasNextLine()) {
			if (lineNumber == 0) { //special case for the time/date of the last time it was updated.
				String dayOfCheckString = sc.nextLine().trim().replaceAll("\\s+", ""); //bug fix to resolve case where only 1 digit in time
				int seperator = dayOfCheckString.indexOf("_");
				String date[] = dayOfCheckString.substring(0, seperator).split("-");
				String time[] = dayOfCheckString.substring(seperator + 1, seperator + 6).split(":");
				int hour = Integer.valueOf(time[0]);
				int minutes = Integer.valueOf(time[1]);
				int year = Integer.valueOf(date[0]);
				int month = Integer.valueOf(date[1]);
				int day = Integer.valueOf(date[2]);
				dayOfCheck = LocalDate.of(year, month, day);
				timeOfDay = LocalTime.of(hour, minutes);
				lineNumber++;
			}
			String input = sc.nextLine();
			String line = input.replaceAll("\\s+", " "); //remove all excessive white space
			if(line.length() < 2){ //sanity check to make sure we aren't processing blank lines.
				line = "";
			}
			if (! (line.isEmpty())) {
				parsedText = line.split(("\\s+"));
				BigInteger freeSpace = new BigInteger(parsedText[1]);
				BigInteger totalSpace = new BigInteger(parsedText[2]);
				driveLetters.get(lineNumber - 1).processValues(freeSpace, totalSpace); //this works because the number of lines correlates to the number of drives that are being parsed.
				lineNumber++;
			}
		}

	}

}
