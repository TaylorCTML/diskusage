import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;

/*
 * Disk Space Monitoring 0.1
 * Author: Taylor Cook
 * Last updated: 2018-07-29
 *
 */

public class DriveLetters {

	private String serverName;
	private Character driveLetter;
	private BigInteger freeSpaceRemaining;
	private BigInteger totalSpace;
	private double percentFreeSpace;

	DriveLetters(Character a, String name) {
		driveLetter = a;
		serverName = name;
	}

	public BigInteger getTotalSpace() {
		return totalSpace;
	}

	private void setTotalSpace(BigInteger totalSpace) {
		this.totalSpace = totalSpace;
	}

	public BigInteger getFreeSpaceRemaining() {
		return freeSpaceRemaining;
	}

	private void setFreeSpaceRemaining(BigInteger freeSpaceRemaining) {
		this.freeSpaceRemaining = freeSpaceRemaining;
	}

	public double getPercentFreeSpace() {
		return percentFreeSpace;
	}

	private void setPercentFreeSpace(double initialPercentFreeSpace) {
		BigDecimal bd = new BigDecimal(initialPercentFreeSpace).setScale(0, RoundingMode.HALF_EVEN); //round the percent to no sig figs
		percentFreeSpace = bd.doubleValue();
	}

	void processValues(BigInteger freeSpace, BigInteger totalSpace) {
		setFreeSpaceRemaining(freeSpace);
		setTotalSpace(totalSpace);
		setPercentFreeSpace(convertBytesToPercent(freeSpace, totalSpace));
	}

	@Override
	public String toString() {
		return serverName + " " + driveLetter + ": " + (int) percentFreeSpace + "% free disk space";
	}

	private double convertBytesToPercent(BigInteger freeSpace, BigInteger totalSpace) {
		double fb = freeSpace.doubleValue();
		double ts = totalSpace.doubleValue();
		return (fb / ts) * 100;
	}

}

